import threading
from time import sleep


class Ticker(object):
    def __init__(self, callback):
        self._interval = 1
        self._callback = callback
        self._thread = None
        self._run()

    def _run(self):
        if self._thread is not None:
            self._callback()
        self._thread = threading.Timer(self._interval, self._run)
        self._thread.start()

    def stop(self):
        self._thread.cancel()



