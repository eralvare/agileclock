class Time(object):
    MAX_SECONDS = 60
    MAX_MINUTES = 60
    MAX_HOURS = 24

    def __init__(self):
        self._seconds = 0
        self._minutes = 0
        self._hours = 0

    def get_seconds(self):
        return self._seconds

    def add_second(self):
        self._seconds += 1

        if (self._seconds == self.MAX_SECONDS):
            self._seconds = 0
            self.add_minute()

    def get_minutes(self):
        return self._minutes

    def add_minute(self):
        self._minutes += 1

        if (self._minutes == self.MAX_MINUTES):
            self._minutes = 0
            self.add_hour()

    def get_hours(self):
        return self._hours

    def add_hour(self):
        self._hours += 1

        if (self._hours == self.MAX_HOURS):
            self._hours = 0

	

