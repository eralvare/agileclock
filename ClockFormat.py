class ClockFormat(object):
    def __init__(self, time):
        self._time = time
        self._pretty_print = "{0}:{1}:{2}"
	self.mode = 24
   
    def get_time(self):
        s = str(self._time.get_seconds()).zfill(2)
        m = str(self._time.get_minutes()).zfill(2)
        h = str(self.get_hours()).zfill(2)
        return self._pretty_print.format(h, m, s, self.get_am_pm())

    def get_hours(self):
        return self._time.get_hours()%self.mode

    def get_am_pm(self):
	return self._time.get_hours()>=12 and 'PM' or 'AM'
	

    def set_mode_12h(self):
        self._pretty_print = "{0}:{1}:{2} {3}"
	self.mode = 12
	
    def set_mode_24h(self):
        self._pretty_print = "{0}:{1}:{2}"
	self.mode = 24
