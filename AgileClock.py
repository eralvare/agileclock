from time import sleep
from ClockFormat import ClockFormat
from Presenter import Presenter
from Ticker import Ticker
from Time import Time


class AgileClock(object):
    def __init__(self):
        self._time = Time()
        ticker = Ticker(self._time.add_second)

        formatted_time = ClockFormat(self._time)
        Presenter(0.5, formatted_time.get_time)

        ticker.stop()


if __name__ == '__main__':
    AgileClock()
