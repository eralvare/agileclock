import random
import os,sys
parentdir = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
sys.path.insert(0,parentdir) 
from ClockFormat import ClockFormat
from Time import Time
import unittest


class ClockFormatTest(unittest.TestCase):
    def setUp(self):
	self.time = Time()
	self.clockFormat = ClockFormat(self.time)

    def add_seconds(self, seconds):
	for i in range(seconds):
            self.time.add_second()

    def test_get_time(self):

        # Arrange
	seconds = 7235+60

        # Act
	self.add_seconds(seconds)

        expected = self.clockFormat.get_time()

        # Assert
        self.assertEqual("02:01:35", expected)

    def test_set_mode_12h(self):
        # Arrange
	seconds = 56750 #15:45:50

        # Act
	self.add_seconds(seconds)

	self.clockFormat.set_mode_12h()
        expected = self.clockFormat.get_time()

        # Assert
        self.assertEqual("03:45:50 PM", expected)
    
    
    def test_get_hours_mode_12h(self):
        # Arrange
	seconds = 56750 #15:45:50

        # Act
	self.add_seconds(seconds)

	self.clockFormat.set_mode_12h()
        expected = self.clockFormat.get_hours()

        # Assert
        self.assertEqual(3, expected)

    def test_get_hours_mode_24h(self):
        # Arrange
	seconds = 56750 #15:45:50

        # Act
	self.add_seconds(seconds)

        expected = self.clockFormat.get_hours()

        # Assert
        self.assertEqual(15, expected)

    def test_set_mode_24h(self):
        # Arrange
	seconds = 56750 #15:45:50
	self.clockFormat.set_mode_12h()

        # Act
	self.add_seconds(seconds)

	self.clockFormat.set_mode_24h()
        expected = self.clockFormat.get_time()

        # Assert
        self.assertEqual("15:45:50", expected)


if __name__ == '__main__':
    unittest.main()
