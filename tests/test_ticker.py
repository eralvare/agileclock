import random
from time import sleep
import unittest
import os,sys
parentdir = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
sys.path.insert(0,parentdir)
from Ticker import Ticker


class TickerTest(unittest.TestCase):
    def setUp(self):
        self.count = Counter()
        self.ticker = Ticker(self.count.add)

    def test_callback_should_be_call_after_one_second(self):
        # Arrange

        # Act
        self.ticker.stop()

        actual = self.count.count
        expected = 0

        self.assertEqual(actual, expected)

    def test_callback_should_be_call_n_times_after_n_seconds(self):
        # Arrange
        floor = 5
        ceil = 10
	n = 1

        # Act
        sleep(n)
        self.ticker.stop()

        expected = n
        actual = self.count.count

        # Assert
        self.assertEqual(actual, expected)


class Counter(object):
    def __init__(self):
        self._count = 0

    def add(self):
        self._count += 1

    @property
    def count(self):
        return self._count
