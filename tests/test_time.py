import random
import os,sys
parentdir = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
sys.path.insert(0,parentdir) 
from Time import Time
import unittest


class TimeTest(unittest.TestCase):
    def setUp(self):
        self.time = Time()

    def test_add_random_seconds(self):

        # Arrange
	seconds = 35

        # Act
        for i in range(seconds):
            self.time.add_second()

        actual = self.time.get_seconds()
        expected = seconds % self.time.MAX_SECONDS

        # Assert
        self.assertEqual(actual, expected)

    def test_add_random_seconds_overflow(self):

        # Arrange
	seconds = 95
	minutes = self.time.get_minutes()

        # Act
        for i in range(seconds):
            self.time.add_second()

        actual = self.time.get_seconds()
        expected = seconds % self.time.MAX_SECONDS

        # Assert
        self.assertEqual(actual, expected)
        self.assertEqual(self.time.get_minutes(), minutes+1)

    def test_add_random_minutes(self):

        # Arrange
	minutes = 42

        # Act
        for i in range(minutes):
            self.time.add_minute()

        actual = self.time.get_minutes()
        expected = minutes

        # Assert
        self.assertEqual(actual, expected)

    def test_add_random_minutes_overflow(self):

        # Arrange
	minutes = 84
	hours = self.time.get_hours()
	
        # Act
        for i in range(minutes):
            self.time.add_minute()

        actual = self.time.get_minutes()
        expected = minutes % self.time.MAX_MINUTES
	

        # Assert
        self.assertEqual(actual, expected)
        self.assertEqual(self.time.get_hours(), hours+1)

    def test_add_random_hour(self):

        # Arrange
        min_number_of_hours = 0
        max_number_of_hours = self.time.MAX_HOURS - 1
        hours = random.randint(min_number_of_hours, max_number_of_hours)

        # Act
        for i in range(hours):
            self.time.add_hour()

        expected = hours
        actual = self.time.get_hours()

        # Assert
        self.assertEqual(actual, expected)

    def test_add_random_hour_overflow(self):

        # Arrange
        min_number_of_hours = self.time.MAX_HOURS
        max_number_of_hours = self.time.MAX_HOURS * 5
        hours = random.randint(min_number_of_hours, max_number_of_hours)

        # Act
        for i in range(hours):
            self.time.add_hour()

        expected = hours % self.time.MAX_HOURS
        actual = self.time.get_hours()

        # Assert
        self.assertEqual(actual, expected)


if __name__ == '__main__':
    unittest.main()
