import gobject
import gtk

gobject.threads_init()


class PresenterWindow(object):
    def __init__(self, refresh_rate, refresh_callback):
        self._refresh_callback = refresh_callback
        self._refresh_rate = refresh_rate

        self._window = gtk.Window()
        self._set_window_properties()
        self._set_window_components()
        self._set_window_handlers()

        self._window.show_all()
        gtk.main()

    def _set_window_properties(self):
        self._window.set_default_size(100, 50)

    def _set_window_components(self):
        self._label = gtk.Label(self._refresh_callback())
        self._window.add(self._label)
        gobject.timeout_add(self._refresh_rate, self.refresh)

    def _set_window_handlers(self):
        self._window.connect("destroy", lambda w: gtk.main_quit())

    def refresh(self):
        self._label.set_label(str(self._refresh_callback()))
        return True
