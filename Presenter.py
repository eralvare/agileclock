from PresenterWindow import PresenterWindow


class Presenter(object):
    def __init__(self, refresh_rate, data_source):
        self._refresh_rate = int((refresh_rate * 1000))
        self._data_source = data_source
        self.print_data()

    def print_data(self):
        PresenterWindow(self._refresh_rate, self._data_source)


